#!/usr/bin/php
<?php
/*
Package requirements (all optional):
- hostapd
- manually compiled jack (remove dbus support)
- vnc4server
- memcache
*/

/* where in /opt are all the files mounted */
//	$folderPrefix = 'majig';
	$folderPrefix = 'naprave';
	
	$wifisubnetwork = 42;
	
	function log_msg($msg) {
		file_put_contents('/tmp/userland.log', time()." $msg\n", FILE_APPEND);
		echo "$msg\n";
	}
	log_msg("starting");
    chdir('/root/npUserland/');

    function setupWifiAP($options, $wlan_interface = 'wlan0', $ssid_suffix = '') {
		global $wifisubnetwork;
		
		$hostapdfilename = 'hostapd_'.$wlan_interface.'.conf';
		$hostapd = file_get_contents('hostapd.conf');

		$hostapd = str_replace('INTERFACE',$wlan_interface, $hostapd);
		$hostapd = str_replace('SSID',$options['ssid_'.$wlan_interface].$ssid_suffix, $hostapd);
		$hostapd = str_replace('PASSWORD',$options['password_'.$wlan_interface], $hostapd);
		
		file_put_contents('/tmp/'.$hostapdfilename, $hostapd);
	
		system('/sbin/ifconfig '.$wlan_interface.' up');
		system('/sbin/ifconfig '.$wlan_interface.' 192.168.'.$wifisubnetwork.'.2/24');
		system('touch /tmp/dhcpd.leases');
		system('/usr/sbin/hostapd /tmp/'.$hostapdfilename.' > /dev/null &');
		//TODO for each interface, not common files
		system('/usr/sbin/dhcpd -cf /root/npUserland/dhcpd.conf -lf /tmp/dhcpd.leases '.$wlan_interface.' --no-pid > /dev/null &');
		$wifisubnetwork++;
    }
    
    $options['wifi'] = 1;
    $options['wifiap'] = 1;
    $options['ssid'] = 'networker';
    $options['password'] = 'networker123';
//     $options['ssid_wlan1'] = 'node';
//     $options['password_wlan1'] = 'password123';
//	$options['clearterm'] = 1;
//	$options['vncserver'] = 1;
//	$options['jack'] = 1;
//	$options['properwebserver'] = 1;
// 	$options['hashwclock'] = 1;
//	$options['wpaclientssid'] = '';
//	$options['wpaclientpass'] = '';

//	$options['wpaclientssid_wlan1'] = '';
//	$options['wpaclientpass_wlan1'] = '';
	$options['dhclient_enp2s0'] = 1;
	$options['dhclient_eth0'] = 1;
	
	//$options['mountalldevices'] = 1;
	
    @include('/opt/'.$folderPrefix.'/app/options.php');
    log_msg('config: '.print_r($options,true));

   if( !file_exists('/tmp/userland.done') ) 
    {
		log_msg("running userland");
		file_put_contents('/tmp/userland.done', 0);

//  		$mac = shell_exec('ifconfig -a | grep \'eth0: \' -A4');
 		$mac = shell_exec('ifconfig -a');
// 		var_dump($mac);
		//$mac = substr($mac, -17);
		$e = strpos($mac, 'ether ' );
// 		var_dump($e);
		$e += 6;
		$mac = substr($mac, $e, 17);
		
		file_put_contents('/tmp/mac', $mac);
		$ssid_suffix = '_'.substr(str_replace(':','', $mac),-4);
// 		print_r($mac); exit;
		//exit;
		
		if( $options['clearterm'] ) {
			//doesnt really work?
			system('setterm -foreground black -background black');
			system('setterm -cursor off;');
		}

		if( $options['hashwclock'] ) {
			log_msg('setting hw clock from rtc');
			system('hwclock -s');
		} else {
			system('date -s "`cat /etc/fake-hwclock.data`"');
		}

		$wifiinterfaces = array();
		if( $options['wifi'] ) {
			log_msg("enabling wifi");
			if( $options['wifiap'] ) {
				log_msg("enabling wifi AP");
				
				foreach($options as $key => $option) {
					if( !$option ) continue;
					if( $key == 'ssid' ) {
						//legacy default wlan
						$wifiinterfaces['wlan0']=1;
						if( !isset($options['ssid_wlan0']) ) {
							$options['ssid_wlan0'] = $options['ssid'];
							$options['password_wlan0'] = $options['password'];
						}
					}
					
					if( strpos($key, 'ssid') === 0 ) {
						$ex = explode('_', $key);
						if( isset($ex[1]) ) {
							//$ex[1];
							$wifiinterfaces[$ex[1]] = 1;
						}
// 						print_r($key);
					}
					
					
					
				}
				print_r($wifiinterfaces);
// 				exit;
				
				foreach( $wifiinterfaces as $wlan_interface => $one ) {
					setupWifiAP($options, $wlan_interface, $ssid_suffix);
				}
			} 
			
			{
				$wifiinterfaces = array();
				//setup connections to other wifi networks
				foreach($options as $key => $option) {
					if( empty($option) ) continue;
					if( $key == 'wpaclientssid' ) {
						//legacy default wlan
						$wifiinterfaces['wlan0']=1;
						$options['wpaclientssid_wlan0'] = $options['wpaclientssid'];
						$options['wpaclientpass_wlan0'] = $options['wpaclientpass'];
					}
					
					if( strpos($key, 'wpaclientssid_') === 0 ) {
						$ex = explode('_', $key);
						if( isset($ex[1]) ) {
							//$ex[1];
							$wifiinterfaces[$ex[1]] = 1;
						}
// 						print_r($key);
					}
				}
				print_r($wifiinterfaces);
 //				exit;

				foreach( $wifiinterfaces as $wlan_interface => $one ) {
					system('/sbin/ifconfig '.$wlan_interface.' up');
					//system('/sbin/ifconfig '.$wlan_interface.' 192.168.42.2/24');
					$wpaconfig = '
					network={
						ssid="'.$options['wpaclientssid_'.$wlan_interface].'"
						psk="'.$options['wpaclientpass_'.$wlan_interface].'"
					}
					';
					file_put_contents('/tmp/wpa_supplicant_'.$wlan_interface.'.conf', $wpaconfig);
					system('wpa_supplicant -B -D wext -i '.$wlan_interface.' -c /tmp/wpa_supplicant_'.$wlan_interface.'.conf');
					sleep(1);
					system('dhclient '.$wlan_interface);
				}
			}
		} else {
			log_msg("shutting down wifi");
			system('/sbin/ifconfig wlan0 down');
			system('/usr/sbin/rfkill block wifi');
		}
		
		//network interface options
		foreach($options as $key => $option) {
			if( !$option ) continue;
			
			if( strpos($key, 'dhcpd_') === 0 ) {
				$ex = explode('_', $key);
				if( isset($ex[1]) ) {
					//$ex[1];
//							$wifiinterfaces[$ex[1]] = 1;
					$intf = $ex[1];
					log_msg("dhcpding $intf");
					system('/usr/bin/touch /tmp/dhcpd_eth'.$intf.'.leases');
					system('/usr/sbin/dhcpd -cf /root/npUserland/dhcpd_eth.conf -lf /tmp/dhcpd_eth'.$intf.'.leases '.$intf.' --no-pid > /dev/null &');
				}
// 						print_r($key);
			}
			
			if( strpos($key, 'dhclient_') === 0 ) {
				$ex = explode('_', $key);
				if( isset($ex[1]) ) {
					$intf = $ex[1];
					log_msg("dhclienting $intf");
					system('/usr/sbin/dhclient '.$intf.' > /dev/null &');
				}
// 						print_r($key);
			}
			
			
		}
		
		
		if( $options['memcache'] ) {
			log_msg("starting memcached");
			system('/usr/bin/memcached -u pi -d');
		}

		log_msg("restoring alsa levels");
		system('/usr/sbin/alsactl restore -L');

		if( $options['jack'] ) {
			$jack_device = 'hw:Device';
			if( !empty($options['jack_device']) ) $jack_device = $options['jack_device'];
			log_msg("starting JACK... $jack_device");
		
			system('/root/jack2/build/linux/jackd -Rv -p 128 -d alsa -d'.$jack_device.' -r 48000 -H -S -n 2 -p 512 -Xseq > /dev/null &');
			sleep(4);
			log_msg("done starting JACK.");
		}
		
		if( $options['vncserver'] ) {
			log_msg("starting vnc server");
			system('/usr/bin/Xvnc4 :1 -rfbauth /root/.vnc/passwd > /dev/null &');
			system('export DISPLAY=:1 ; xterm > /dev/null &');
		}

		if( $options['mountalldevices'] ) {
			sleep(2);//omg mount already
			system('/bin/mount -a');
			sleep(3);
	//      mount("/dev/sda1", "/data", "auto", 0, "ro");
			system("/bin/mount /dev/mmcblk0p4 /data -o ro");
			system("/bin/mount /dev/sda1 /data2 -o ro");
			system("/bin/mount /dev/sda2 /data3 -o ro");
			system("/bin/mount /dev/sda /data4 -o ro");
		}
		
		
		
		
		if(file_exists('/data/master') || file_exists('/data2/master') || file_exists('/data3/master') || file_exists('/data4/master') ) {
			log_msg("running as master");
			system('touch /tmp/dhcpd_eth.leases');
			system('ifconfig eth0 192.168.41.1/24');
			system('/usr/sbin/dhcpd -cf /root/npUserland/dhcpd_eth.conf -lf /tmp/dhcpd_eth.leases eth0 --no-pid > /dev/null &');
		} else {
			log_msg("running as slave");
			if( trim(file_get_contents('/sys/class/net/eth0/carrier')) == "1" ) {
				log_msg("getting ip with dhclient");
				sleep(1);
				system('dhclient eth0');
			}
		}

		if( $options['properwebserver'] ) {
			log_msg("starting apache web server");
			system('/usr/sbin/apachectl start');
		} else {
			log_msg("starting php web server");
			system('/usr/bin/php -S 0.0.0.0:80 -t /opt/'.$folderPrefix.'/app/www/ > /dev/null &');
		}

		file_put_contents('/tmp/userland.done', 1);
	}

	if( file_exists('/opt/'.$folderPrefix.'/app') ) {
		log_msg("Running app");
		system('/opt/'.$folderPrefix.'/app/start.php >/dev/null &');
	} else {
		log_msg("Running basic app");
		system('/opt/'.$folderPrefix.'/start.php >/dev/null &');
	}

	for( $i=2;$i<10; $i++ ) {
		$appname = "app$i";
		if( file_exists("/opt/".$folderPrefix."/$appname") ) {
			log_msg("Running secondary apps: ".$appname);
			system("/opt/".$folderPrefix."/$appname/start.php >/dev/null &");
		}
	}

 	system('/usr/sbin/ntpdate pool.ntp.org');
/*
system('echo 25 > /sys/class/gpio/export');
sleep(1);
system('echo out > /sys/class/gpio/gpio25/direction');
sleep(1);
/**/
